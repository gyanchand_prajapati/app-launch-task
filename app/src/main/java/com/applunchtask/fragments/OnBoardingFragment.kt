package com.applunchtask.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.applunchtask.R
import com.applunchtask.databinding.FragmentOnBoardingBinding
import com.applunchtask.handler.OnBoardingHandler


class OnBoardingFragment : Fragment() {
    private lateinit var binding:FragmentOnBoardingBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_on_boarding, container, false)
        binding.handler = OnBoardingHandler(this)
        return binding.root
    }

    companion object {

        @JvmStatic
        fun newInstance() = OnBoardingFragment()

    }
}