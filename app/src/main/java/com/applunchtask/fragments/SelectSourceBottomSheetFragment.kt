package com.applunchtask.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.applunchtask.R
import com.applunchtask.databinding.ImagePickerBottomSheetBinding
import com.applunchtask.interfaces.OnSelectOptionListener
import com.google.android.material.bottomsheet.BottomSheetDialogFragment


class SelectSourceBottomSheetFragment(private val onSelectOptionListener: OnSelectOptionListener) :
    BottomSheetDialogFragment() {
    lateinit var imagePickerBottomSheetBinding: ImagePickerBottomSheetBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        imagePickerBottomSheetBinding =
            DataBindingUtil.inflate(inflater, R.layout.image_picker_bottom_sheet, container, false)
        init()
        return imagePickerBottomSheetBinding.root
    }


    private fun init() {
        imagePickerBottomSheetBinding.cameraImage.setOnClickListener {
            onSelectOptionListener.onOptionSelect("camera")
        }

        imagePickerBottomSheetBinding.galleryImage.setOnClickListener {
            onSelectOptionListener.onOptionSelect("gallery")
        }

        imagePickerBottomSheetBinding.closeDialog.setOnClickListener {
            dismiss()
        }
    }


}