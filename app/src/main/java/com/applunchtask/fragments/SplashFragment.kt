package com.applunchtask.fragments

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.applunchtask.R
import com.applunchtask.utils.AppPreference


class SplashFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onAttach(context: Context) {
        Handler(Looper.getMainLooper()).postDelayed({
            if(AppPreference.getPreferenceValueByKey(context,AppPreference.email)!=""){
                findNavController()
                    .navigate(R.id.action_splashFragment_to_usersFragment)
            }else{
                findNavController()
                    .navigate(R.id.action_splashFragment_to_onBoardingFragment)
            }

        },3000)
        super.onAttach(context)
    }



}