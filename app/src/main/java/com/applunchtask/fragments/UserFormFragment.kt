package com.applunchtask.fragments

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.applunchtask.R
import com.applunchtask.databinding.FragmentUserFormBinding
import com.applunchtask.handler.UserAddHandler
import com.applunchtask.respository.UserRepository
import com.applunchtask.utils.ImagePicker
import com.applunchtask.utils.MyApplication
import com.applunchtask.utils.Utils
import com.applunchtask.view_model.UserViewModel
import com.applunchtask.view_model.UserViewModelFactory
import com.lassi.common.utils.KeyUtils
import com.lassi.data.media.MiMedia
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException


class UserFormFragment : Fragment() {
    lateinit var fragmentUserFormBinding: FragmentUserFormBinding
    private val userViewModel: UserViewModel by viewModels {
        UserViewModelFactory((requireActivity().application as MyApplication).repository,this)
    }
    lateinit var userRepository: UserRepository
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fragmentUserFormBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_user_form, container, false)
        fragmentUserFormBinding.data = userViewModel
        fragmentUserFormBinding.handler = UserAddHandler(this)
        return fragmentUserFormBinding.root
    }


    @RequiresApi(Build.VERSION_CODES.R)
     val receiveData = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if (it.resultCode == Activity.RESULT_OK) {
            val selectedMedia =
                it.data?.getSerializableExtra(KeyUtils.SELECTED_MEDIA) as ArrayList<MiMedia>
            if (!selectedMedia.isNullOrEmpty()) {
                val bitmap: Bitmap?
                val resultUri = Uri.parse(selectedMedia[0].path)
                try {
                    bitmap = BitmapFactory.decodeFile(selectedMedia[0].path)
                    fragmentUserFormBinding.userProfilePic.setImageBitmap(bitmap)
                    val stream = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.PNG, 50, stream)
                    userViewModel.byteArray = stream.toByteArray()
                    userViewModel.isFileSelected = true

                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }


        }



    }

    @RequiresApi(Build.VERSION_CODES.R)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ImagePicker.CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == AppCompatActivity.RESULT_OK) {
               // CropUtils.beginCrop(this.requireActivity(), Uri.parse(ImagePicker.photoFile!!.toURI().toString()))
                val originalSize = File(ImagePicker.photoFile!!.absolutePath).length()
                println("original file length is $originalSize")
                val takenImage = Utils.getResizedBitmap(BitmapFactory.decodeFile(ImagePicker.photoFile!!.absolutePath),200)
                fragmentUserFormBinding.userProfilePic.setImageBitmap(takenImage)
                val stream = ByteArrayOutputStream()
                takenImage!!.compress(Bitmap.CompressFormat.PNG, 90, stream)
                userViewModel.byteArray = stream.toByteArray()
                userViewModel.isFileSelected = true
            } else { // Result was a failure

            }
        }

        /*if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK){

            val selectedMedia = data?.getSerializableExtra(KeyUtils.SELECTED_MEDIA) as ArrayList<MiMedia>
            if (!selectedMedia.isNullOrEmpty()) {
                val bitmap: Bitmap?
                val resultUri = Uri.parse(selectedMedia[0].path)
                try {
                    bitmap = BitmapFactory.decodeFile(selectedMedia[0].path)
                    fragmentUserFormBinding.userProfilePic.setImageBitmap(bitmap)
                    val stream = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream)
                    userViewModel.byteArray = stream.toByteArray()
                    userViewModel.isFileSelected = true

                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }

           *//* if(data!=null){
                //val result = CropImage.getActivityResult(data)
                var bitmap: Bitmap?
                val bundle = data.extras
                if (bundle != null) {
                    for (key in bundle.keySet()) {
                        println( key + " : " + if (bundle[key] != null) bundle[key] else "NULL")
                    }
                }



               *//**//* bitmap = if (Build.VERSION.SDK_INT < 28) {
                    MediaStore.Images.Media.getBitmap(this.requireActivity().contentResolver, resultUri)
                } else {
                    val source = ImageDecoder.createSource(this.requireActivity().contentResolver, resultUri)
                    ImageDecoder.decodeBitmap(source)
                }

                fragmentUserFormBinding.userProfilePic.setImageBitmap(bitmap)
                val stream = ByteArrayOutputStream()
                bitmap!!.compress(Bitmap.CompressFormat.PNG, 90, stream)
                userViewModel.byteArray = stream.toByteArray()
                userViewModel.isFileSelected = true*//**//*
            }*//*


        }*/
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Utils.onRequestPermissionsResult(requestCode,permissions,grantResults,requireActivity())
    }

    companion object {
        @JvmStatic
        fun newInstance() = UserFormFragment()
    }
}