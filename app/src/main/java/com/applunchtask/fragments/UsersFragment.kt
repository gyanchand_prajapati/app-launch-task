package com.applunchtask.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.applunchtask.R
import com.applunchtask.adapter.UserListAdapter
import com.applunchtask.databinding.FragmentUsersBinding
import com.applunchtask.handler.SwipeDeleteHandler
import com.applunchtask.handler.UserListHandler
import com.applunchtask.interfaces.OnUserSelectListener
import com.applunchtask.local_db.entities.User
import com.applunchtask.utils.MyApplication
import com.applunchtask.utils.Utils
import com.applunchtask.view_model.UserViewModel
import com.applunchtask.view_model.UserViewModelFactory


class UsersFragment : Fragment() {
    lateinit var fragmentUsersBinding: FragmentUsersBinding
    private val userViewModel: UserViewModel by viewModels {
        UserViewModelFactory((requireActivity().application as MyApplication).repository, this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fragmentUsersBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_users, container, false)
        fragmentUsersBinding.handler = UserListHandler(this)
        init()
        return fragmentUsersBinding.root
    }

    private fun init() {
        userViewModel.allUsers.observe(viewLifecycleOwner) {
            if (it.isNotEmpty()) {
                fragmentUsersBinding.isDataAvailable = true
                setUpUserList(it as ArrayList<User>)
            } else {
                fragmentUsersBinding.isDataAvailable = false
                Utils.showToast(requireContext(), "Data not found")
            }

        }
    }


    private fun setUpUserList(userList: ArrayList<User>) {
        fragmentUsersBinding.localUserListRv.layoutManager = LinearLayoutManager(requireContext())
        val userListAdapter = UserListAdapter(requireContext(), userList,this)
        fragmentUsersBinding.localUserListRv.adapter = userListAdapter


        val swipeDeleteHandler = object : SwipeDeleteHandler(requireContext()) {

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                when (direction) {
                    ItemTouchHelper.LEFT -> {
                        userViewModel.deleteUser(userList[viewHolder.adapterPosition])
                        userListAdapter.deleteItem(viewHolder.adapterPosition)
                    }
                }

            }
        }

        val touchHelper = ItemTouchHelper(swipeDeleteHandler)
        touchHelper.attachToRecyclerView(fragmentUsersBinding.localUserListRv)


    }


    companion object {
        @JvmStatic
        fun newInstance() = UsersFragment()
    }


}