package com.applunchtask.fragments

import android.annotation.SuppressLint
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.applunchtask.R
import com.applunchtask.databinding.FragmentWeatherBinding
import com.applunchtask.handler.WeatherUIHandler
import com.applunchtask.utils.Utils
import com.applunchtask.view_model.WeatherViewModel
import com.applunchtask.web_service.Status
import com.google.android.gms.location.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.util.concurrent.TimeUnit


class WeatherFragment : Fragment() {
    lateinit var fragmentWeatherBinding: FragmentWeatherBinding
    lateinit var weatherViewModel: WeatherViewModel
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    @RequiresApi(Build.VERSION_CODES.R)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fragmentWeatherBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_weather, container, false)
        weatherViewModel = WeatherViewModel(requireContext())
        fragmentWeatherBinding.utils = Utils
        fragmentWeatherBinding.handler = WeatherUIHandler(this)
        init()
        return fragmentWeatherBinding.root
    }


    @SuppressLint("MissingPermission")
    @RequiresApi(Build.VERSION_CODES.R)
    private fun init() {
        fusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(requireActivity())

        if (Utils.checkingPermissionIsEnabledOrNot(requireContext())) {
            locationRequest = LocationRequest().apply {
                interval = TimeUnit.SECONDS.toMillis(60)
                fastestInterval = TimeUnit.SECONDS.toMillis(30)
                maxWaitTime = TimeUnit.MINUTES.toMillis(2)
                priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            }

            locationCallback = object : LocationCallback() {
                override fun onLocationResult(p0: LocationResult) {
                    super.onLocationResult(p0)
                    p0.lastLocation.let {
                        weatherViewModel.getWeatherInfo(
                            p0.lastLocation.latitude,
                            p0.lastLocation.longitude
                        )

                    }
                }
            }

            fusedLocationProviderClient.requestLocationUpdates(
                locationRequest, locationCallback,
                Looper.myLooper()!!
            )

        } else {
            Utils.goToPermissionSetting(requireActivity())
        }







        lifecycleScope.launch {
            weatherViewModel.weatherState.collect { it ->
                when (it.status) {
                    Status.LOADING -> {

                    }

                    Status.SUCCESS -> {
                        it.data.let {
                            fragmentWeatherBinding.data = it
                        }
                    }
                    // In case of error, show some data to user
                    else ->
                        Utils.showToast(requireContext(), "${it.message}")
                }
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = WeatherFragment()
        const val LOCATION_REQUEST = 100
        const val GPS_REQUEST = 101
        const val TAG = "Location removed::::WeatherFragment"
    }


    override fun onDestroy() {
        super.onDestroy()
        val removeTask = fusedLocationProviderClient.removeLocationUpdates(locationCallback)
        removeTask.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                Log.d(TAG, "Location Callback removed.")
            } else {
                Log.d(TAG, "Failed to remove Location Callback.")
            }
        }
    }


}