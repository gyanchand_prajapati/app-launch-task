package com.applunchtask.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.applunchtask.R
import com.applunchtask.databinding.FragmentLoginBinding
import com.applunchtask.handler.LoginHandler
import com.applunchtask.view_model.LoginViewModel


class LoginFragment : Fragment() {
    lateinit var fragmentLoginBinding: FragmentLoginBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        fragmentLoginBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        fragmentLoginBinding.data = LoginViewModel(requireActivity())
        fragmentLoginBinding.handler = LoginHandler(this)
        return fragmentLoginBinding.root

    }

    companion object {
        @JvmStatic
        fun newInstance() = LoginFragment()

    }
}