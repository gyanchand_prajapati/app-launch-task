package com.applunchtask.handler

import androidx.navigation.fragment.findNavController
import com.applunchtask.R
import com.applunchtask.fragments.OnBoardingFragment

class OnBoardingHandler(val onBoardingFragment: OnBoardingFragment) {

    fun onLoginClick(){
        onBoardingFragment.findNavController().navigate(R.id.action_onBoardingFragment_to_loginFragment)
    }
}