package com.applunchtask.handler

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.navigation.fragment.findNavController
import com.applunchtask.R
import com.applunchtask.fragments.SelectSourceBottomSheetFragment
import com.applunchtask.fragments.UserFormFragment
import com.applunchtask.interfaces.OnSelectOptionListener
import com.applunchtask.utils.ImagePicker
import com.applunchtask.utils.Utils
import com.applunchtask.view_model.UserViewModel
import com.lassi.common.utils.KeyUtils
import com.lassi.data.media.MiMedia
import com.lassi.domain.media.LassiOption
import com.lassi.domain.media.MediaType
import com.lassi.presentation.builder.Lassi
import java.io.IOException


class UserAddHandler(private val userFormFragment: UserFormFragment) : OnSelectOptionListener {
    private lateinit var userViewModel: UserViewModel
    private lateinit var selectSourceBottomSheetFragment: SelectSourceBottomSheetFragment
    fun onClickSave(userViewModel: UserViewModel) {
        this.userViewModel = userViewModel
        userViewModel.isValidation(userFormFragment)
    }

    @RequiresApi(Build.VERSION_CODES.R)
    fun onClickImage() {
        if (!Utils.checkingPermissionIsEnabledOrNot(userFormFragment.requireContext())) {
            Utils.requestMultiplePermission(
                userFormFragment.requireActivity(),
                requestPermissionCode
            )
        } else {
            selectSourceBottomSheetFragment = SelectSourceBottomSheetFragment(this)
            selectSourceBottomSheetFragment.show(
                userFormFragment.childFragmentManager,
                "selectSourceBottomSheetFragment"
            )
        }
    }


    fun onClickCancel() {
        userFormFragment.findNavController().navigate(R.id.action_userFormFragment_to_usersFragment)
    }


    companion object {
        const val requestPermissionCode = 101
    }

    override fun onOptionSelect(option: String) {
        if (option == "camera") {
            selectSourceBottomSheetFragment.dismiss()
            ImagePicker.onCaptureImage(userFormFragment)
        } else {
            selectSourceBottomSheetFragment.dismiss()
            val intent = Lassi(userFormFragment.requireContext())
                .with(LassiOption.GALLERY)
                .setMaxCount(1)
                .setGridSize(3)
                .setMediaType(MediaType.IMAGE)
                .setCompressionRation(10)
                .build()
            userFormFragment.receiveData.launch(intent)
        }
    }





}