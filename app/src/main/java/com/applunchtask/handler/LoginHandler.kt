package com.applunchtask.handler

import androidx.navigation.fragment.findNavController
import com.applunchtask.R
import com.applunchtask.fragments.LoginFragment
import com.applunchtask.utils.AppPreference
import com.applunchtask.utils.Utils
import com.applunchtask.view_model.LoginViewModel

class LoginHandler(private val loginFragment: LoginFragment) {
    private lateinit var loginViewModel: LoginViewModel
    fun onLoginClick(loginViewModel: LoginViewModel) {
        this.loginViewModel = loginViewModel
        if (loginViewModel.isValidation(loginFragment)) {
                if(loginViewModel.email==" testapp@google.com" || loginViewModel.password=="Test@123456"){
                    AppPreference.savePreference(loginFragment.requireContext(),AppPreference.email,loginViewModel.email)
                    loginFragment.findNavController().navigate(R.id.action_loginFragment_to_usersFragment)
                }else{
                    Utils.showSnackBarOnError(
                        loginFragment.fragmentLoginBinding.loginEmail,
                        loginFragment.resources.getString(R.string.incorrect_message),
                        loginFragment.requireContext()
                    )
                }
        }
    }
}