package com.applunchtask.handler

import androidx.navigation.fragment.findNavController
import com.applunchtask.R
import com.applunchtask.fragments.UsersFragment
import com.applunchtask.view_model.UserViewModel


class UserHandler(private val usersFragment: UsersFragment) {

    fun onClickUser() {
        usersFragment.findNavController().navigate(R.id.action_usersFragment_to_weatherFragment)
    }


}