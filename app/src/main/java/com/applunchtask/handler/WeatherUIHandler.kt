package com.applunchtask.handler

import androidx.navigation.fragment.findNavController
import com.applunchtask.R
import com.applunchtask.fragments.WeatherFragment
import com.applunchtask.utils.AppPreference


class WeatherUIHandler(private val weatherFragment: WeatherFragment) {

    fun onClickBack() {
        weatherFragment.findNavController().navigate(R.id.action_weatherFragment_to_usersFragment)
    }

    fun onClickLogout() {
        AppPreference.logout(weatherFragment)

    }

}