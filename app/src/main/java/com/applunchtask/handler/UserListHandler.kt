package com.applunchtask.handler

import androidx.navigation.fragment.findNavController
import com.applunchtask.R
import com.applunchtask.fragments.UsersFragment
import com.applunchtask.view_model.UserViewModel


class UserListHandler(private val usersFragment: UsersFragment) {
    private lateinit var userViewModel: UserViewModel

    fun onClickSave(userViewModel: UserViewModel) {
        this.userViewModel = userViewModel

    }


    fun onClickAddUser() {
        usersFragment.findNavController().navigate(R.id.action_usersFragment_to_userFormFragment)
    }

    fun onClickUser() {
        usersFragment.findNavController().navigate(R.id.action_usersFragment_to_weatherFragment)
    }


}