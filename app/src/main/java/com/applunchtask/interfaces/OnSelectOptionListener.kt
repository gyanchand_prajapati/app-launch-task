package com.applunchtask.interfaces

interface OnSelectOptionListener {
    fun onOptionSelect(option:String)
}