package com.applunchtask.interfaces

interface OnUserSelectListener {
    fun onUserSelect()
}