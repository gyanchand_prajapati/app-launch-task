package com.applunchtask.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.applunchtask.R
import com.applunchtask.databinding.ItemUserListLayoutBinding
import com.applunchtask.fragments.UsersFragment
import com.applunchtask.handler.UserHandler
import com.applunchtask.handler.UserListHandler
import com.applunchtask.interfaces.OnUserSelectListener
import com.applunchtask.local_db.entities.User


class UserListAdapter(
    private val mContext: Context,
    private val usersList: ArrayList<User>,
    private val usersFragment: UsersFragment
) : RecyclerView.Adapter<UserListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(mContext).inflate(R.layout.item_user_list_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = usersList[position]
        holder.mBinding.data = user
        holder.mBinding.handler = UserHandler(usersFragment)

    }

    override fun getItemCount(): Int = usersList.size


    fun deleteItem(position:Int){
        usersList.removeAt(position)
        notifyDataSetChanged()
    }




    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val mBinding = ItemUserListLayoutBinding.bind(itemView)
    }
}