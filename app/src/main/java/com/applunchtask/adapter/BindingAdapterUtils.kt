package com.applunchtask.adapter

import android.graphics.BitmapFactory
import android.widget.ImageView
import androidx.databinding.BindingAdapter


class BindingAdapterUtils {
    companion object {


        @JvmStatic
        @BindingAdapter(value = ["imageArray", "placeholder"], requireAll = false)
        fun setImage(view: ImageView, imageArray: ByteArray?, placeholder: String?) {
            //Utils.setImage(view.context, view, imageUrl!!)
            if(imageArray!=null){
                val bitmap = BitmapFactory.decodeByteArray(imageArray, 0, imageArray.size)
                view.setImageBitmap(bitmap)
            }

        }


    }
}