package com.applunchtask.local_db.dao

import androidx.room.*
import com.applunchtask.local_db.entities.User
import kotlinx.coroutines.flow.Flow


@Dao
interface UsersDao {
    @Query("SELECT * FROM user")
    fun getAllUserList(): Flow<List<User>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(users: User)

    @Delete
    suspend fun delete(user: User)
}