package com.applunchtask.local_db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class User(
    @ColumnInfo(name = "first_name") var firstName: String?,
    @ColumnInfo(name = "last_name") var lastName: String?,
    @ColumnInfo(name = "email") var email: String?,
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB) var image: ByteArray? = null
){
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}
