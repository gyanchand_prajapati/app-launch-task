package com.applunchtask.web_service


import com.applunchtask.model.WeatherModel
import com.applunchtask.view_model.WeatherViewModel
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*


interface APIInterface {

    @GET("onecall")
    suspend fun getWeatherInfo(
        @Query("lat") lat : Double,
        @Query("lon") lon : Double,
        @Query("units") units : String,
        @Query("appid") appId : String
    ): WeatherModel




}