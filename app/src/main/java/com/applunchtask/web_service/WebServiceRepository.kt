package com.applunchtask.web_service

import android.app.Application
import android.content.Context
import com.applunchtask.R
import com.applunchtask.model.WeatherModel
import com.applunchtask.view_model.WeatherViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn


class WebServiceRepository(private val context: Context) {
    private var apiInterface: APIInterface = APIClient.makeRetrofitService()
    private var networkHelper: NetworkHelper = NetworkHelper(context)

    suspend fun getWeatherInfo(
        latitude: Double,
        longitude: Double
    ): Flow<ResourceState<WeatherModel>> {
        return flow {
            if(networkHelper.isNetworkConnected()){
                val response = apiInterface.getWeatherInfo(
                    latitude,
                    longitude,
                    "imperial",
                    context.getString(R.string.appId)
                )
                emit(ResourceState.success(response))
            }else{
                emit(ResourceState.error("No internet connection"))
            }

        }.flowOn(Dispatchers.IO)
    }


}


