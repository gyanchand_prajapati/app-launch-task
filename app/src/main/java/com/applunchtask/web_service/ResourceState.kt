package com.applunchtask.web_service


data class ResourceState<out T>(val status: Status, val data: T?, val message: String?) {

    companion object {


        fun <T> success(data: T?): ResourceState<T> {
            return ResourceState(Status.SUCCESS, data, null)
        }


        fun <T> error(msg: String): ResourceState<T> {
            return ResourceState(Status.ERROR, null, msg)
        }


        fun <T> loading(): ResourceState<T> {
            return ResourceState(Status.LOADING, null, null)
        }
    }
}


enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}