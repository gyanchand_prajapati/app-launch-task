package com.applunchtask.respository

import androidx.annotation.WorkerThread
import com.applunchtask.local_db.dao.UsersDao
import com.applunchtask.local_db.entities.User
import kotlinx.coroutines.flow.Flow

class UserRepository (private val usersDao: UsersDao) {
    val userList: Flow<List<User>> = usersDao.getAllUserList()

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(user: User) {
        usersDao.insert(user)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun deleteUser(user: User) {
        usersDao.delete(user)
    }
}