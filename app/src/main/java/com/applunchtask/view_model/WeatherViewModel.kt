package com.applunchtask.view_model

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.applunchtask.model.CurrentWeatherModel
import com.applunchtask.model.WeatherModel
import com.applunchtask.web_service.ResourceState
import com.applunchtask.web_service.Status
import com.applunchtask.web_service.WebServiceRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class WeatherViewModel(context: Context) : ViewModel(){
    private val webServiceRepository = WebServiceRepository(context)
    val weatherState = MutableStateFlow(ResourceState(Status.LOADING, WeatherModel(), ""))

    fun getWeatherInfo(lat: Double,lang:Double) {
        weatherState.value = ResourceState.loading()

        viewModelScope.launch {
            webServiceRepository.getWeatherInfo(lat,lang)
                .catch {
                    weatherState.value = ResourceState.error(it.message.toString())
                }
                .collect{
                    weatherState.value = ResourceState.success(it.data)
                }
        }
    }






}



