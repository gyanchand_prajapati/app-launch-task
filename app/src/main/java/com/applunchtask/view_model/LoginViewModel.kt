package com.applunchtask.view_model

import android.app.Activity
import androidx.databinding.BaseObservable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.applunchtask.R
import com.applunchtask.fragments.LoginFragment
import com.applunchtask.utils.Utils
import com.applunchtask.utils.Utils.isValidEmail
import java.util.HashMap

class LoginViewModel(activity: Activity) : BaseObservable() {
    var email: String = ""
    var password: String = ""

    fun isValidation(loginFragment: LoginFragment): Boolean {
        val isFormValidated: Boolean
        when {
            email == "" -> {
                isFormValidated = false
                Utils.showSnackBarOnError(
                    loginFragment.fragmentLoginBinding.loginEmail,
                    loginFragment.resources.getString(R.string.please_enter_email),
                    loginFragment.requireContext()
                )
            }

            !isValidEmail(email) -> {
                isFormValidated = false
                Utils.showSnackBarOnError(
                    loginFragment.fragmentLoginBinding.loginEmail,
                    loginFragment.resources.getString(R.string.please_enter_valid_email),
                    loginFragment.requireContext()
                )
            }
            password == "" -> {
                isFormValidated = false
                Utils.showSnackBarOnError(
                    loginFragment.fragmentLoginBinding.loginPassword,
                    loginFragment.resources.getString(R.string.please_enter_password),
                    loginFragment.requireContext()
                )
            }

            else -> {
                isFormValidated = true
            }
        }

        return isFormValidated
    }






}