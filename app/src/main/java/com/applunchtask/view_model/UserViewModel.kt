package com.applunchtask.view_model

import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import androidx.navigation.fragment.findNavController
import com.applunchtask.R
import com.applunchtask.fragments.UserFormFragment
import com.applunchtask.fragments.UsersFragment
import com.applunchtask.local_db.entities.User
import com.applunchtask.respository.UserRepository
import com.applunchtask.utils.Utils
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch

class UserViewModel(private val repository: UserRepository,private val fragment: Fragment) : ViewModel() {
    var firstName: String = ""
    var lastName: String = ""
    var email: String = ""
    var isFileSelected: Boolean = false
    var byteArray: ByteArray? = null
    val allUsers: LiveData<List<User>> = repository.userList.asLiveData()

    fun isValidation(userFormFragment: UserFormFragment) {
        when {

            firstName == "" -> {

                Utils.showSnackBarOnError(
                    userFormFragment.fragmentUserFormBinding.firstNameEt,
                    userFormFragment.resources.getString(R.string.please_enter_first_name),
                    userFormFragment.requireContext()
                )
            }

            lastName == "" -> {

                Utils.showSnackBarOnError(
                    userFormFragment.fragmentUserFormBinding.userLastName,
                    userFormFragment.resources.getString(R.string.please_enter_last_name),
                    userFormFragment.requireContext()
                )
            }

            email == "" -> {

                Utils.showSnackBarOnError(
                    userFormFragment.fragmentUserFormBinding.userEmail,
                    userFormFragment.resources.getString(R.string.please_enter_email),
                    userFormFragment.requireContext()
                )
            }

            !Utils.isValidEmail(email) -> {

                Utils.showSnackBarOnError(
                    userFormFragment.fragmentUserFormBinding.userEmail,
                    userFormFragment.resources.getString(R.string.please_enter_valid_email),
                    userFormFragment.requireContext()
                )
            }
            !isFileSelected -> {

                Utils.showSnackBarOnError(
                    userFormFragment.fragmentUserFormBinding.userEmail,
                    userFormFragment.resources.getString(R.string.please_select_the_image),
                    userFormFragment.requireContext()
                )
            }

            else -> {
                if(insertDataInfoDb(User(firstName, lastName, email, byteArray))){
                    Utils.showToast(userFormFragment.requireContext(),"data inserting...")
                }else{
                    userFormFragment.findNavController().navigate(R.id.action_userFormFragment_to_usersFragment)
                }
            }
        }


    }

    private fun insertDataInfoDb(user: User):Boolean {
        var isActive:Boolean = false
        viewModelScope.launch {
            repository.insert(user)
            isActive =  this.isActive
        }
        return isActive
    }

    fun deleteUser(user: User){
        viewModelScope.launch {
            repository.deleteUser(user)
        }
    }



}

class UserViewModelFactory(private val repository: UserRepository,val fragment: Fragment) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(UserViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return UserViewModel(repository,fragment) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}