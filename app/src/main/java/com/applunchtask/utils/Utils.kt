package com.applunchtask.utils

import android.Manifest
import android.Manifest.permission.*
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Typeface
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.Settings
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.applunchtask.R
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import java.io.ByteArrayOutputStream
import java.io.File


object Utils {
    var RequestPermissionCode = 101

    fun showToast(mContext: Context, message: String) {
        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show()
    }

    fun isValidEmail(email: String): Boolean {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email)
            .matches()
    }


    fun hideSystemUI(activity: Activity) {
        val window = activity.window.decorView.setSystemUiVisibility(
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        )
    }


    fun showSnackBarOnError(view: View, message: String, context: Context) {
        val snackBar = Snackbar.make(view, message, Snackbar.LENGTH_LONG)
        val topView = snackBar.view
        val params = topView.layoutParams as FrameLayout.LayoutParams
        params.gravity = Gravity.TOP
        topView.layoutParams = params
        snackBar.changeFont()
        snackBar.setBackgroundTint(ContextCompat.getColor(context, R.color.color_20213A))
        snackBar.setTextColor(ContextCompat.getColor(context, R.color.white))
        snackBar.show()
    }

    fun isValidPassword(password: String?): Boolean {
        password?.let {
            val passwordPattern =
                "^(?=.*[0-9])(?=.*[A-Z].*)(?=.*[a-z])(?=.*[!@#\$%^&*+=?-]).{8,15}$"
            val passwordMatcher = Regex(passwordPattern)

            return passwordMatcher.find(password) != null
        } ?: return false
    }


    fun showSnackBarOnSuccess(view: View, message: String, context: Context, colorCode: Int) {
        val snackBar = Snackbar.make(
            view, message, Snackbar.LENGTH_LONG
        )
        snackBar.changeFont()
        snackBar.setBackgroundTint(ContextCompat.getColor(context, colorCode))
        snackBar.setTextColor(ContextCompat.getColor(context, R.color.white))
        snackBar.show()
    }

    fun showSnackBarErrorFromTop(view: View, message: String, context: Context) {
        val snackBarView = Snackbar.make(view, message, Snackbar.LENGTH_LONG)
        snackBarView.changeFont()
        val snackView = snackBarView.view
        val params = snackView.layoutParams as FrameLayout.LayoutParams
        params.gravity = Gravity.TOP
        snackView.layoutParams = params
        snackView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorGoogle))
        snackBarView.setTextColor(ContextCompat.getColor(context, R.color.white))
        snackBarView.animationMode = BaseTransientBottomBar.ANIMATION_MODE_FADE
        snackBarView.show()
    }

    fun showSnackBarSuccessFromTop(view: View, message: String, context: Context) {
        val snackBarView = Snackbar.make(view, message, Snackbar.LENGTH_LONG)
        snackBarView.changeFont()
        val snackView = snackBarView.view
        val params = snackView.layoutParams as FrameLayout.LayoutParams
        params.gravity = Gravity.TOP
        snackView.layoutParams = params
        snackView.setBackgroundColor(ContextCompat.getColor(context, R.color.color_201F1E))
        snackBarView.setTextColor(ContextCompat.getColor(context, R.color.white))
        snackBarView.animationMode = BaseTransientBottomBar.ANIMATION_MODE_FADE
        snackBarView.show()
    }

    private fun Snackbar.changeFont() {
        val tv = view.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
        val font = Typeface.createFromAsset(context.assets, "graphikmedium.otf")
        tv.typeface = font
    }

    fun hideKeyboards(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = activity.currentFocus
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun hideKeyboard(mContext: Context, editText: EditText?) {
        val imm =
            mContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
    }

    fun hideSoftKeyboard(activity: Activity) {
        val inputMethodManager =
            activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        if (inputMethodManager.isActive) {
            if (activity.currentFocus != null) {
                inputMethodManager.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
            }
        }
    }


    @SuppressLint("ObsoleteSdkInt")
    fun isLocationPermission(mActivity: Activity): Boolean {
        /* check run time dynamic permission */
        return if (Build.VERSION.SDK_INT >= 23) {
            mActivity.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
        } else {
            true
        }
    }

    fun goToPermissionSetting(mActivity: Activity) {
        try {
            val intent = Intent()
            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            val uri = Uri.fromParts("package", mActivity.packageName, null)
            intent.data = uri
            mActivity.startActivity(intent)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    @RequiresApi(Build.VERSION_CODES.R)
    fun checkingPermissionIsEnabledOrNot(mContext: Context): Boolean {
        val cameraPermission = mContext.packageManager.checkPermission(CAMERA, mContext.packageName)
        val locationPermission = mContext.packageManager.checkPermission(ACCESS_FINE_LOCATION, mContext.packageName)
        return cameraPermission == PackageManager.PERMISSION_GRANTED && locationPermission == PackageManager.PERMISSION_GRANTED
    }

    @RequiresApi(Build.VERSION_CODES.R)
    fun requestMultiplePermission(activity: Activity, requestPermissionCode: Int) {
        ActivityCompat.requestPermissions(
            activity,
            arrayOf(CAMERA, ACCESS_FINE_LOCATION, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE),
            requestPermissionCode
        )
    }

    fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray,
        activity: Activity
    ) {
        when (requestCode) {
            RequestPermissionCode -> if (grantResults.isNotEmpty()) {
                val cameraPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED
                val readStoragePermission = grantResults[1] == PackageManager.PERMISSION_GRANTED
                val writeStoragePermission = grantResults[2] == PackageManager.PERMISSION_GRANTED
                val locationPermission = grantResults[3] == PackageManager.PERMISSION_GRANTED
                if (cameraPermission && readStoragePermission && writeStoragePermission && locationPermission) {
                    Toast.makeText(activity, "Permission Granted", Toast.LENGTH_LONG)
                        .show()
                } else {
                    //requestMultiplePermission(activity)
                }
            }
        }
    }

     fun convertFahrenheitToCelsius(fahrenheit:Double):Double{
        return ((fahrenheit  -  32) * 5)/9
    }


    fun bitmapToFile(bitmap: Bitmap, fileNameToSave: String): ByteArray? { // File name like "image.png"
        //create a file to write bitmap data
        var file: File? = null
        var bitmapdata:ByteArray? = null
        return try {
            file = File(Environment.getExternalStorageDirectory().toString() + File.separator + fileNameToSave)
            file.createNewFile()

            //Convert bitmap to byte array
            val bos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos) // YOU can also save it in JPEG
            bitmapdata = bos.toByteArray()

            bitmapdata
            //write the bytes in file
          /*  val fos = FileOutputStream(file)
            fos.write(bitmapdata)
            fos.flush()
            fos.close()
            file*/
        } catch (e: Exception) {
            e.printStackTrace()
            bitmapdata // it will return null
        }
    }


    fun getResizedBitmap(image: Bitmap, maxSize: Int): Bitmap? {
        var width = image.width
        var height = image.height
        val bitmapRatio = width.toFloat() / height.toFloat()
        if (bitmapRatio > 1) {
            width = maxSize
            height = (width / bitmapRatio).toInt()
        } else {
            height = maxSize
            width = (height * bitmapRatio).toInt()
        }
        return Bitmap.createScaledBitmap(image, width, height, true)
    }

}