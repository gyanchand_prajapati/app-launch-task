package com.applunchtask.utils

import android.app.Application
import com.applunchtask.local_db.AppDatabase
import com.applunchtask.respository.UserRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class MyApplication : Application() {
    val applicationScope = CoroutineScope(SupervisorJob())

    val database by lazy { AppDatabase.getDatabase(this) }
    val repository by lazy { UserRepository(database.users()) }
}