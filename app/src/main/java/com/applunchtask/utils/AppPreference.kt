package com.applunchtask.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.applunchtask.R


object AppPreference {
    private var preferenceName: String = "com.applunchtask"
    var email = "email"


    fun savePreference(mContext: Context, key: String, value: String) {
        val sharedPreferences: SharedPreferences =
            mContext.getSharedPreferences(preferenceName, Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putString(key, value)
        editor.apply()
        editor.commit()
    }

    fun getPreferenceValueByKey(mContext: Context, key: String): String? {
        val sharedPreferences: SharedPreferences =
            mContext.getSharedPreferences(preferenceName, Context.MODE_PRIVATE)
        return sharedPreferences.getString(key, "")
    }

    fun saveIntPreference(mContext: Context, key: String, value: Int) {
        val sharedPreferences: SharedPreferences =
            mContext.getSharedPreferences(preferenceName, Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putInt(key, value)
        editor.apply()
        editor.commit()
    }

    fun getIntPreferenceValueByKey(mContext: Context, key: String): Int? {
        val sharedPreferences: SharedPreferences =
            mContext.getSharedPreferences(preferenceName, Context.MODE_PRIVATE)
        return sharedPreferences.getInt(key, 0)
    }


    fun logout(fragment:Fragment) {
        val sharedPreferences: SharedPreferences =
            fragment.requireContext().getSharedPreferences(preferenceName, Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.clear()
        editor.apply()
        editor.commit()
        fragment.findNavController().navigate(R.id.action_weatherFragment_to_loginFragment)
    }


}